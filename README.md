# Docker Alpine Curl

Basic docker with curl installed

## Build

    docker build -t cannafo/alpine-curl .

## Usage

    docker run cannafo/alpine-curl https://cannafo.com
